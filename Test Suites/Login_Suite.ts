<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Login_Suite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>c6f7984d-ebc8-4762-bc08-593767a4fcaa</testSuiteGuid>
   <testCaseLink>
      <guid>1ac31346-0f07-45b4-b985-aa0a3c436d6c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC_Login</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>9100ff31-8d6b-4ea7-b2b5-09683cebe232</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/input_data_loginSuite</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>9100ff31-8d6b-4ea7-b2b5-09683cebe232</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>6f7883bc-5836-4c57-b5fc-77a90ae92ee3</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>9100ff31-8d6b-4ea7-b2b5-09683cebe232</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>688078c7-1f2e-45e3-89f1-f1842dab9c6f</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
