<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Geo_Suite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>c06f80bb-011b-4490-b04f-aa9045685c26</testSuiteGuid>
   <testCaseLink>
      <guid>24366145-035f-47c1-bdba-801ee60ea39c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login_c</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>e9b01caf-a374-4dcd-b9a6-f2d3416d77ce</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/dataGeo_suite</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>e9b01caf-a374-4dcd-b9a6-f2d3416d77ce</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>80b3c105-2025-4ed6-95ac-8c81cf4852ef</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>e9b01caf-a374-4dcd-b9a6-f2d3416d77ce</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>31e3e14e-b6ba-4f06-92c6-ee6b19be15e9</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>5b128ab1-9505-479b-a83c-88c24bf7186b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/singlecase/TC_Geo</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>a6b992c8-f5cb-4725-8def-3f6cbb9707be</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/singlecase/close_App</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
